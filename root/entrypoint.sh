#!/usr/bin/env sh
set -euo pipefail

if [ ! -f "$MPD_CONFIG_FILE" ]; then
    envsubst < /etc/mpd/mpd.conf.in > "$MPD_CONFIG_FILE"
fi

find "$MPD_EXTRA_CONFIG_DIR" -type f -iname '*.conf' -exec cat "{}" \; >> "$MPD_CONFIG_FILE"

mpd --no-daemon "$MPD_CONFIG_FILE"
